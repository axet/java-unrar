# java-unrar

RAR 4 support

Features over standart repo:

- maven support
- NativeFile / NativeStorage - java File class wrapper, which allows use unrar with InputStreams and FileDiscriptors (useful on Android)

# Exmples

Live example can be found in Torrent Client:

  * https://gitlab.com/axet/android-torrent-client/blob/master/app/src/main/java/com/github/axet/torrentclient/app/TorrentPlayer.java

# Unrar

```java
    File local = new File("test.rar");
    Archive archive = new Archive(new NativeStorage(local));
    List<FileHeader> list = archive.getFileHeaders();
    for (FileHeader header : list) {
        System.out.println(header.isDirectory());
        System.out.println(header.getFileNameW());
        System.out.println(header.getFileNameString());
        archive.extractFile(header, new FileOutputStream(new File("out.txt")));
    }
```

# Central Maven Repo

```xml
    <dependency>
      <groupId>com.github.axet</groupId>
      <artifactId>java-unrar</artifactId>
      <version>1.7.0-8</version>
    </dependency>
```

# Android Studio

```gradle
    compile ('com.github.axet:java-unrar:1.7.0-8') { exclude group: 'commons-logging', module: 'commons-logging' }
```
