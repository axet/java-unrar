package de.innosystec.unrar;

import java.io.File;
import java.io.FileNotFoundException;

public class NativeStorage {
    protected File f;

    public NativeStorage(File f) {
        this.f = f;
    }

    public NativeStorage(NativeStorage v) {
        f = new File(f.getPath());
    }

    public NativeFile read() throws FileNotFoundException {
        return new NativeFile(f, "r");
    }

    public NativeStorage open(String name) {
        return new NativeStorage(new File(f, name));
    }

    public boolean exists() {
        return f.exists();
    }

    public NativeStorage getParent() {
        return new NativeStorage(f.getParentFile());
    }

    public long length() {
        return f.length();
    }

    public String getPath() {
        return f.getPath();
    }
}
