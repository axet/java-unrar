package de.innosystec.unrar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class NativeFile {
    protected RandomAccessFile r;

    public NativeFile() {
    }

    public NativeFile(File f, String mode) throws FileNotFoundException {
        this.r = new RandomAccessFile(f, mode);
    }

    public void setPosition(long s) throws IOException {
        r.seek(s);
    }

    public int read() throws IOException {
        return r.read();
    }

    public int readFully(byte[] buf, int len) throws IOException {
        r.readFully(buf, 0, len);
        return len;
    }

    public int read(byte[] buf, int off, int len) throws IOException {
        return r.read(buf, off, len);
    }

    public long getPosition() throws IOException {
        return r.getFilePointer();
    }

    public void close() throws IOException {
        r.close();
    }

}
